package org.smart4j.framework.bean;

/**
 * 返回数据对象
 * Created by feng.ding on 2016/9/23.
 */
public class Data {

    private Object model;

    public Data(Object model) {
        this.model = model;
    }

    public Object getModel() {
        return model;
    }

    public void setModel(Object model) {
        this.model = model;
    }
}
