package org.smart4j.framework.proxy;

/**
 * Created by feng.ding on 2016/9/26.
 */
public interface Proxy {

    Object doProxy(ProxyChain proxyChain) throws Throwable;
}
