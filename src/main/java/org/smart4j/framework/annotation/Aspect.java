package org.smart4j.framework.annotation;

import java.lang.annotation.*;

/**
 * Created by feng.ding on 2016/9/26.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {

    /**
     * 注解
     * @return
     */
    Class<? extends Annotation> value();
}
