package org.smart4j.framework.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by feng.ding on 2016/9/23.
 */
public final class StringUtil {

    /**
     * 判断字符串是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        if(str != null){
            str = str.trim();
        }
        return StringUtils.isEmpty(str);
    }

    public static String[] splitString(String str, String separatorChar ){
        return StringUtils.split(str, separatorChar);
    }
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }
}

