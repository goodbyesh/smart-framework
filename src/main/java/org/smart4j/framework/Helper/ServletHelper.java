package org.smart4j.framework.Helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by feng.ding on 2016/9/27.
 */
public final class ServletHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServletHelper.class);

    /**
     * 每个线程独自拥有一份ServletHelper对象
     */
    private static final ThreadLocal<ServletHelper> SERVLET_HELPER_HOLDER = new ThreadLocal<ServletHelper>();

    private HttpServletRequest request;
    private HttpServletResponse response;

    private ServletHelper(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    /**
     * 初始化
     * @param request
     * @param response
     */
    public static void init(HttpServletRequest request, HttpServletResponse response){
        SERVLET_HELPER_HOLDER.set(new ServletHelper(request, response));
    }

    /**
     * 销毁
     */
    public static void destory(){
        SERVLET_HELPER_HOLDER.remove();
    }

    /**
     * 获取request对象
     * @return
     */
    private static HttpServletRequest getRequest(){
        return SERVLET_HELPER_HOLDER.get().request;
    }

    /**
     * 获取response对象
     * @return
     */
    private static HttpServletResponse getResponse(){
        return SERVLET_HELPER_HOLDER.get().response;
    }

    /**
     * 获取session对象
     * @return
     */
    private static HttpSession getSession(){
        return getRequest().getSession();
    }

    public static void setRequestAttribute(String key, Object value){
        getRequest().setAttribute(key, value);
    }

    public static <T> T getRequestAttribute(String key){
        return (T) getRequest().getAttribute(key);
    }

    public static void removeRequestAttribute(String key){
        getRequest().removeAttribute(key);
    }

    /**
     * 发送重定向
     * @param location
     */
    public static void sendRedirect(String location){
        try{
            getResponse().sendRedirect(getRequest().getContextPath() + location);
        } catch (IOException e) {
            LOGGER.error("redirect failure", e);
        }
    }

    public static void setSessionAttribute(String key, Object value){
        getSession().setAttribute(key, value);
    }

    public static <T> T getSessionAttribute(String key){
        return (T) getSession().getAttribute(key);
    }

    public static void removeSessionAttribute(String key){
        getSession().removeAttribute(key);
    }

    public static void invalidateSession(){
        getSession().invalidate();
    }

}
