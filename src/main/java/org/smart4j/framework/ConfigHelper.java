package org.smart4j.framework;

import org.smart4j.framework.util.PropUtil;

import java.sql.Connection;
import java.util.Properties;

/**
 * Created by feng.ding on 2016/9/23.
 */
public final class ConfigHelper {

    private static final Properties CONFIG_PROPS = PropUtil.loadProps(ConfigConstant.CONFIG_FILE);

    public static String getJdbcDriver(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_DRIVER);
    }

    public static String getJdbcUrl(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_URL);

    }

    public static String getJdbcUsername(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_USERNAME);
    }


    public static String getJdbcPassword(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_PASSWORD);
    }

    /**
     * 获取基础包名
     * @return
     */
    public static String getAppBasePackage(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.APP_BASE_PACKAGE);
    }

    /**
     * 获取Jsp路径
     * @return
     */
    public static String getAppJspPath(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.APP_JSP_PATH, "/WEB-INF/view/");
    }

    /**
     * 获取静态资源路径
     * @return
     */
    public static String getAppAssetPath(){
        return PropUtil.getString(CONFIG_PROPS, ConfigConstant.APP_ASSET_PATH, "/asset/");
    }
}
