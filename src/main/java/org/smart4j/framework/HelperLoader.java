package org.smart4j.framework;

import org.smart4j.framework.Helper.AopHelper;
import org.smart4j.framework.Helper.BeanHelper;
import org.smart4j.framework.Helper.ClassHelper;
import org.smart4j.framework.Helper.IocHelper;
import org.smart4j.framework.util.ClassUtil;

/**
 * 加载相应的Heler类
 * Created by feng.ding on 2016/9/23.
 */
public final class HelperLoader {

    public static void init(){
        Class<?>[] classList = {
                ClassHelper.class,
                BeanHelper.class,
                AopHelper.class,
                IocHelper.class,
                ClassHelper.class
        };
        for(Class<?> cls : classList){
            ClassUtil.loadClass(cls.getName(), false);
        }
    }
}
